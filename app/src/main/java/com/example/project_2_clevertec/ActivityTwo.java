package com.example.project_2_clevertec;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

public class ActivityTwo extends AppCompatActivity {


    ArrayList<Icon> icons = new ArrayList<Icon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setInitialData();
        RecyclerView recyclerView = findViewById(R.id.list);

        Adapter.OnClickListener clickListener = new Adapter.OnClickListener() {


            @Override
            public void onStateClick(Icon icon, int position) {
                Intent intent;
                intent = new Intent(ActivityTwo.this, MainActivity.class);
                startActivity(intent);
            }
        };
        Adapter adapter = new Adapter(this, icons, clickListener);
        recyclerView.setAdapter(adapter);
    }


    private void setInitialData() {
        icons.add(new Icon("Title Page 2", "Description 2", R.mipmap.ic_people_1));
    }
}