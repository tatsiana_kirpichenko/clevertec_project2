package com.example.project_2_clevertec;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter  extends RecyclerView.Adapter<Adapter.ViewHolder>{

    interface OnClickListener{
        void onStateClick(Icon icon, int position);
    }

    private final OnClickListener onClickListener;

    private final LayoutInflater inflater;
    private final List<Icon> icons;

    Adapter(Context context, List<Icon> icons, OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        this.icons = icons;
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter.ViewHolder holder, int position) {
        Icon icon = icons.get(position);
        holder.image.setImageResource(icon.getImage());
        holder.title.setText(icon.getTitle());
        holder.description.setText(icon.getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                onClickListener.onStateClick(icon, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return icons.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView image;
        final TextView title, description;
        ViewHolder(View view){
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
        }
    }
}

